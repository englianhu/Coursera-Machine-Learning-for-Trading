
<img src='文艺坊图库/coursera.jpg' height='100'> <img src='文艺坊图库/NYIF.png' height='100'>

------

[<img src='文艺坊图库/RStudioCloud.png' height='20'>](https://rstudio.cloud) [<img src='文艺坊图库/RStudioCom2.png' height='20'>](https://community.rstudio.com/new-topic?category=shiny&tags=shiny) [![](文艺坊图库/shiny-badge.svg)](https://www.shinyapps.io)

![](https://raw.githubusercontent.com/englianhu/Coursera-Machine-Learning-for-Trading/%E4%B8%96%E5%8D%9A%E9%87%8F%E5%8C%96%E7%A0%94%E7%A9%B6%E9%99%A2/%E6%96%87%E8%89%BA%E5%9D%8A%E5%9B%BE%E5%BA%93/%E6%97%B6%E5%85%89%E6%9C%BA%E5%99%A8%2001.gif)

*The Time Machine 2002 (时光机器 2002)*

- [悦诗风吟：蔡卓宜按脸秘方](https://www.douyin.com/user/MS4wLjABAAAAgnpmCWEdda8Wauzuo_MTsC-UjxjbRlUNUSkA55Ecot75KfHulPYUuYTXts0uzPS7?modal_id=7005994517736262949)
- [揭秘全球量化投资之父詹姆斯·西蒙斯](https://open.toutiao.com/a7151346713263948325/?a_t=AHzmbrJ6CcFZ5Bb6e4ZHTBgS2B6kq493SPGCZe4PHKXZiDXJKraeg2A11zZZAtmTmUU2Crwk3&biz_log=B3fR4jutc96MzeDHNcmYzb7iUUzu2LBAjxBoiZrkX9i6umjKEKS1en7BdSPgmUjkZvFKq82g2wFtYxETJbGxufL&crypt=7198&device_brand=&dt=Redmi+7A&gy=cc1072953bd514b388e5175566825bac10a66f494272f91362dd82514b21141646040422457cc5f6e8f4ecfc878c2b5b96c47f93f42581c604dd05a3106057ee556885deee282f85349574cad79201da30d1b7d812a41fc14b0407c9d3df3eea652a365c00132c62b745a6666a271c4587d61f1eb6aa9bddafe4d34899df6a5c&item_id=7151346713263948325&label=ovm_tab_default_content_feed_1_5_v1&req_id=20221226203945D255F0A510D94B271843&utm_campaign=open&utm_medium=webview&utm_source=mi_llq_api&docid=7151346713263948325&cp=cn-toutiao&itemtype=news&version=2&mibusinessId=miuibrowser&env=production&category=news_finance&cateCode=%E8%B4%A2%E7%BB%8F)

**大秦赋 (Chinese Emperor)**<br>
春秋战国《*礼记•经解*》<br>
孔子曰：『君子慎始，差若毫厘，缪以千里。』

> <span style='color:#FFEBCD; background-color:#D2B48C;'>**《礼记·经解》孔子曰：**</span><span style='color:#A9A9A9'; background-color:#696969;'>*「君子慎始。差若毫厘，谬以千里。」*</span>[^1]

*引用：[「快懂百科」《礼记•经解》](https://www.baike.com/wikiid/2225522569881832051?view_id=2tt3iw3blkq000)和[第一范文网：差之毫厘，谬以千里的故事](https://www.diyifanwen.com/chengyu/liuziyishangchengyugushi/2010051523105152347092749890.htm)和[「百度百科」春秋时期孔子作品《礼记•经解》](https://baike.baidu.com/item/%E7%A4%BC%E8%AE%B0%C2%B7%E7%BB%8F%E8%A7%A3/2523092)和[「當代中國」差之毫釐 謬以千里](https://www.ourchinastory.com/zh/2962/%E5%B7%AE%E4%B9%8B%E6%AF%AB%E9%87%90%20%E8%AC%AC%E4%BB%A5%E5%8D%83%E9%87%8C)*

[^1]: [HTML Color Codes](https://html-color.codes)

# [Coursera Machine Learning for Trading 专项课程](https://www.coursera.org/specializations/machine-learning-trading?)

Start Your Career in Machine Learning for Trading. Learn the machine learning techniques used in quantitative trading.

## 目录

Specialization : [Machine Learning for Trading Specialization](https://www.coursera.org/specializations/machine-learning-trading)

- Course 1 : [......](h)
- Course 2 : [......]()
- Course 3 : [......]()

## 课程 1 : [Introduction to Trading, Machine Learning & GCP](https://www.coursera.org/learn/introduction-trading-machine-learning-gcp?specialization=machine-learning-trading)

  - 第1周 : Introduction to Trading with Machine Learning on Google Cloud
  - 第2周 : Supervised Learning with BigQuery ML
  - 第3周 : Time Series and ARIMA Modeling
  - 第4周 : Introduction to Neural Networks and Deep Learning


## 课程 2 :  [Using Machine Learning in Trading and Finance](https://www.coursera.org/learn/machine-learning-trading-finance)

  - 第1周 : Introduction to Quantitative Trading and `TensorFlow`
    - 第1.1章 : Introduction to `TensorFlow`, Trading, ML
      - 第1.1.1章 : [**视频:** Introduction to Course](https://www.youtube.com/watch?v=HR9InPaahA0)
      - 第1.1.2章 : **教材:** Welcome to Using Machine Learning in Trading and Finance
      
    - 第1.2章 : Understand Quantitative Trading Strategies
      - 第1.2.1章 : [**视频:** Basic Trading Strategy Entries and Exits Endogenous Exogenous](https://www.youtube.com/watch?v=Ffmcq2HZz30)
      - 第1.2.2章 : [**视频:** Basic Trading Strategy Building a Trading Model](https://www.youtube.com/watch?v=5kfVeUCa9EM)
      - 第1.2.3章 : [**视频:** Advanced Concepts in Trading Strategies](https://www.youtube.com/watch?v=0dN8xWv674A)
      - 第1.2.4章 : **测试:** Understand Quantitative Strategies
      
    - 第1.3章 : Introduction to `TensorFlow`
      - 第1.3.1章 : [**视频:** Overview](https://www.youtube.com/watch?v=odC8-RAnslc)
      - 第1.3.2章 : [**视频:** Introduction to `TensorFlow`](https://www.youtube.com/watch?v=_nNksFwPbZk)
      - 第1.3.3章 : [**视频:** `TensorFlow` API Hierarchy](https://www.youtube.com/watch?v=5FHCRO22MzI)
      - 第1.3.4章 : [**视频:** Components of `tensorflow` Tensors and Variables](https://www.youtube.com/watch?v=JuHPqzq6glU)
      - 第1.3.5章 : [**视频:** Getting Started with Google Cloud Platform and Qwiklabs](https://www.youtube.com/watch?v=G1fs73llt_w)
      - 第1.3.6章 : [**视频:** Lab Intro Writing low-level `TensorFlow` programs](https://www.youtube.com/watch?v=Chkgaa-k7aA)
      - 第1.3.7章 : **评估:** Lab: Writing low-level `TensorFlow` Programs
      - 第1.3.8章 : [**视频:** Working in-memory and with files](https://www.youtube.com/watch?v=LKlDWW7Coxo)
      - 第1.3.9章 : [**视频:** Training on Large Datasets with `tf.data` API](https://www.youtube.com/watch?v=xgyRV6E02F4)
      - 第1.3.10章 : [**视频:** Getting the data ready for model training](https://www.youtube.com/watch?v=IXXEfAJK-x4)
      - 第1.3.11章 : [**视频:** Embeddings](https://www.youtube.com/watch?v=kpBPZwaxqFY)
      - 第1.3.12章 : [**视频:** Lab Intro Manipulating data with `TensorFlow` Dataset API](https://www.youtube.com/watch?v=GmOpvxA0vI0)
      - 第1.3.13章 : **评估:** Lab: Manipulating data with `TensorFlow` Dataset API

  - 第2周 : Training neural networks with `Tensorflow 2` and `Keras`
    - 第2.1章 : Overview of Neural Networks and Introduction to `Keras` APIs
      - 第2.1.1章 : [**视频:** Overview](https://www.youtube.com/watch?v=LT_ZafVRSJ4)
      - 第2.1.2章 : [**视频:** Activation functions](https://www.youtube.com/watch?v=hkFLCMDpd0A)
      - 第2.1.3章 : [**视频:** Activation functions: Pitfalls to avoid in Backpropagation(https://www.youtube.com/watch?v=ztYFTRqI-Tw)
      - 第2.1.4章 : [**视频:** Neural Networks with `Keras` Sequential API](https://www.youtube.com/watch?v=qWChK51Jitk)
      - 第2.1.5章 : [**视频:** Serving models in the cloud](https://www.youtube.com/watch?v=uKfckc5JnYw)
      - 第2.1.6章 : [**评估:** Lab Intro : `Keras` Sequential API](https://www.youtube.com/watch?v=6HysgE56ps4)
      - 第2.1.7章 : [**视频:** Neural Networks with `Keras` Functional API](https://www.youtube.com/watch?v=0PdTjYd2yiM)
      - 第2.1.8章 : [**视频:** Regularization: The Basics](https://www.youtube.com/watch?v=MEwWjLUHWZc)
      - 第2.1.9章 : [**视频:** Regularization: L1, L2, and Early Stopping](https://www.youtube.com/watch?v=xjaRhdOBung)
      - 第2.1.10章 : [**视频:** Regularization: Dropout](https://www.youtube.com/watch?v=x78vXgiVWfg)
      - 第2.1.11章 : [**评估:** Lab Intro: `Keras` Functional API](https://www.youtube.com/watch?v=TeXjXzldKYM)
      - 第2.1.12章 : [**视频:** Recap](https://www.youtube.com/watch?v=RLjIS43vpyM)
      
  - 第3周 : Build a Momentum-based Trading System
    - 第3.1章 : Identify momentum-based factors
      - 第3.1.1章 : [**视频:** Introduction to Momentum Trading](https://www.youtube.com/watch?v=ADRrKM2VfEQ)
      - 第3.1.2章 : [**视频:** Introduction to Hurst](https://www.youtube.com/watch?v=LCUC1e4MmZU)
      - 第3.1.3章 : **教材:** Hurst Exponent and Trading Signals Derived from Market Time Series
    - 第3.2章 : Build a trading model that uses momentum factors
      - 第3.2.1章 : [**视频:** Building a Momentum Trading Model](https://www.youtube.com/watch?v=ceuGOyrqmis)
      - 第3.2.2章 : [**视频:** Define the Problem](https://www.youtube.com/watch?v=Y8teYAEaAxo)
      - 第3.2.3章 : [**视频:** Collect the Data](https://www.youtube.com/watch?v=pq7QmLzRJFc)
      - 第3.2.4章 : [**视频:** Creating Features](https://www.youtube.com/watch?v=Wkcnb4sV-X0)
      - 第3.2.5章 : [**视频:** Split the Data](https://www.youtube.com/watch?v=PkiHSzcS8qc)
      - 第3.2.6章 : [**视频:** Selecting a Machine Learning Algorithm](https://www.youtube.com/watch?v=NCh0iC8QgSg)
      - 第3.2.7章 : [**视频:** Backtest on Unseen Data](https://www.youtube.com/watch?v=R5O1Hwkx--4)
      - 第3.2.8章 : [**视频:** Understanding the Code: Simple ML Strategies to Generate Trading Signal](https://www.youtube.com/watch?v=qQnV-fpJLxA)
      - 第3.2.9章 : **交流:** Compare interpretability versus explanatory power of the momentum factor
      - 第3.2.10章 : [**视频:** Lab Intro: Momentum Trading](https://www.youtube.com/watch?v=_v2FEwPo_mg)
      - 第3.2.11章 : **评估:** Lab: Momentum Strategies
      - 第3.2.12章 : [**视频:** Momentum Trading Lab Solution](https://www.youtube.com/watch?v=MDkaCD80aIg)
      - 第3.2.13章 : **评估:** Optional Lab: Improve Momentum Trading strategies using Hurst
  - 第4周 : Build a Pair Trading Strategy Prediction Model
    - 第4.1章 : Picking Pairs
      - 第4.1.1章 : [**视频:** Introduction to Pair Trading](https://www.youtube.com/watch?v=0Tpt6ahWa7g)
      - 第4.1.2章 : [**视频:** Picking Pairs](https://www.youtube.com/watch?v=JdbjPjVECUc)
      - 第4.1.3章 : [**视频:** Picking Pairs with Clustering](https://www.youtube.com/watch?v=ld7Yki6vZAk)
    - 第4.2章 : Trading Strategy
      - 第4.2.1章 : [**视频:** How to implement a Pair Trading Strategy](https://www.youtube.com/watch?v=kuINXkdE1Us)
      - 第4.2.2章 : [**视频:** Evaluate Results of a Pair Trade](https://www.youtube.com/watch?v=trXXLzeDjzw)
    - 第4.3章 : Backtesting and Avoiding Overfitting
      - 第4.3.1章 : [**视频:** Backtesting and Avoiding Overfitting](https://www.youtube.com/watch?v=lYX8LXwQjmc)
      - 第4.3.2章 : [**视频:** Next Steps: Imrovements to your Pair Strategy](https://www.youtube.com/watch?v=sBzvrEMNGZE)
      - 第4.3.3章 : [**视频:** Lab Intro: Pairs Trading](https://www.youtube.com/watch?v=uGNf3C3HeDA)
      - 第4.3.4章 : **评估:** Lab: Pairs Trading Strategy
      - 第4.3.5章 : [**视频:** Lab Solution: Pairs Trading](https://www.youtube.com/watch?v=9XZZz7TYE18)
    - 第4.4章 : Optimize momentum trading model to minimize costs
      - 第4.4.1章 : [**视频:** Kalman Filter Introduction](https://www.youtube.com/watch?v=3fylBOJeleo)
      - 第4.4.2章 : [**视频:** Kalman Filter Trading Applications](https://www.youtube.com/watch?v=hmKwjuZLpqg)
      - 第4.4.3章 : **测试:** Pairs Trading Strategy concepts
      - 第4.4.4章 : **评估:** Optional Lab: Estimate parameters using Kalman Filters


## 课程 3 : [Reinforcement Learning for Trading Strategies](https://www.coursera.org/learn/trading-strategies-reinforcement-learning?specialization=machine-learning-trading)

  - 第1周 : Introduction to Course and Reinforcement Learning
  - 第2周 : Neural Network Based Reinforcement Learning
  - 第3周 : Portfolio Optimization

## 附录

There have a `RMarkdown` file and also a copy of `pdf` and `png` format certificate of accomplishment in every single course (folders inside repo).

![Awarded on `08 June 2022`](.......png

Above `png` file and feel free to download [.......pdf](.......pdf).

## Coursera Verified Certificate（考试啦认证文凭）

Certificate of Specialization : [......](https://www.coursera.org/account/accomplishments/specialization/......)

- Cert 1 : [......](https://www.coursera.org/account/accomplishments/records/......)
- Cert 2 : [......](https://www.coursera.org/account/accomplishments/records/......)
- Cert 3 : [......](https://www.coursera.org/account/accomplishments/records/......)
- Cert 4 : [......](https://www.coursera.org/account/accomplishments/records/......)
- University : [New York Institute of Finance](https://www.nyif.com)

## 参考文献

- [ML for Trading - 2nd Edition (GitHub)](https://github.com/englianhu/machine-learning-for-trading) ❤️‍🔥
- [算法交易中的机器学习系列（一）](https://zhuanlan.zhihu.com/p/262260494) ❤️‍🔥
- [Machine Learning for Algorithmic Trading (Official)](https://ml4trading.io) ❤️‍🔥
- [ML for Trading (Exchange)](https://exchange.ml4trading.io) ❤️‍🔥
- [Machine Learning for Algorithmic Trading (2nd Edition).pdf](https://raw.githubusercontent.com/englianhu/Coursera-Machine-Learning-for-Trading/8fdfbfdec4ffc277afe658dcbcd1846bea8b3e1c/reference/Machine%20Learning%20for%20Algorithmic%20Trading%20(2nd%20Edition).pdf) ❤️‍🔥

---

<img src='文艺坊图库/NYIF.png' width='40'> [纽约金融学院 — 华尔街的百年学府](https://www.nyif.com)
